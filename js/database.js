$(document).ready(function () {
    refresh();
    showEvents();
});

// ROUTER URL
var url = 'router.php';

$('body').on('focus', "#datepicker", function () {
    $(this).datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });
});

$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    }
});

$(document).on('click', '#getLogin', function () {
    var email = $('#inputEmail').val();
    var password = $('#inputPassword').val();
    if (email == null || email == "") {
        Swal.fire('Molimo unesite email adresu');
    } else if (password == null || password == "") {
        Swal.fire('Molimo unesite zaporku');
    } else {
        login();
    }
})

$(document).on('click', '#logoutBtn', function () {
    logout();
});

$(document).on('click', '#dodajEvent', function () {
    var datum = $('#datepicker').val();
    var opis = $('#opis').val();
    var link = $('#link').val();

    if (false) { }
    else {
        $.ajax({
            type: 'POST',
            url: url,
            data: { "projekt": "p_dplatisa", "procedura": "p_dodaj_event", "datum": datum, "opis": opis, "link": link },
            success: function (data) {
                var jsonBody = JSON.parse(data);
                var errcode = jsonBody.h_errcode;
                var message = jsonBody.h_message;
                if ((message == null || message == "") && (errcode == null || errcode == 0)) {

                } else {

                }
                refresh();
                showEvents();
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            async: true
        });
    }
})

function login() {
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_common", "procedura": "p_login", "username": $('#inputEmail').val(), "password": $('#inputPassword').val() },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcod = jsonBody.h_errcod;
            var message = jsonBody.h_message;
            if (message == null || message == "", errcod == null || errcod == 0) {
                $("#logged").load("content.html");
            } else {

            }
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true
    });
}

function refresh() {
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_common", "procedura": "p_refresh" },
        success: function (data) {
            console.log(data);
            var jsonBody = JSON.parse(data);
            if (jsonBody.h_errcode == 999) {
                $("#logged").html(loginForm());
            }
            else {
                $("#logged").load("content.html");
            }
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

function logout() {
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_common", "procedura": "p_logout" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            if (message == null || message == "" || errcode == null) {

            } else {
                location.reload();
            }
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true
    });
}

function loginForm() {
    var output = "<h1>Molimo prijavite se za pregled i unos eventa.</h1>";
    output += "<br><form><div class='form-group'>";
    output += "<label for='inputEmail'>Email adresa</label>";
    output += "<input type='email' class='form-control' id='inputEmail' aria-describedby='emailHelp' placeholder='Enter email'></div>";
    output += "<div class='form-group'><label for='inputPassword'>Password</label>";
    output += "<input type='password' class='form-control' id='inputPassword' placeholder='Password'></div>";
    output += "<button class='btn btn-primary' id='getLogin'>Submit</button></form>";
    return output;
}

function showEvents() {
    var events = [];
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_dplatisa", "procedura": "p_get_events" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    var Datum = new Date(v.datum);
                    events.push({ 'Date': new Date(Datum.getFullYear(), Datum.getMonth(), Datum.getDate()), 'Title': v.opis, 'Link': v.link })
                });
                $("#caleandar").html("");
                var settings = {};
                var element = document.getElementById('caleandar');
                caleandar(element, events, settings);
            } else {
                if (errcode == 999) {
                    $("#logged").html(loginForm());
                } else {
                }
            }
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true
    });
}