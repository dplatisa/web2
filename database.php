<?php
//zapisani su podaci za bazu i koristi se dolje u funkciji za instanciranje baze
require_once('db_credential.php');
require_once('poruke.php');

// dohvaćanje evenata iz baze
function f_get_events($db, $in_obj){
    $sql = "SELECT * FROM EVENTS";
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

// spremanje eventa u bazu
function f_save_event($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    
    $sql = "INSERT INTO EVENTS (datum, opis, link) VALUES ('$in_obj->datum', '$in_obj->opis', '$in_obj->link')";
    
    $db->set_charset("utf8");
    if ($db->query($sql) === TRUE) {
        echo $insert_pass;
    }else{
        echo $insert_error;   
    } 
}

// login 
function f_login($db, $in_obj){
    global $wrong_login;
    //provjerava da li postoji mail i sifra..
    $sql = "SELECT * FROM USERS WHERE EMAIL = '$in_obj->username' AND PASSWORD = '$in_obj->password' ";
    $rows=[];
    $db->set_charset("utf8");    
    $result = $db->query($sql);
    while($row = mysqli_fetch_assoc($result)) {
        $rows[]=$row;
    }
    if (!empty($rows)){
        $_SESSION["USERNAME"] = $in_obj->username;
        echo '{"data":' . json_encode($rows) . '}';
    }else{
        echo $wrong_login;
    }
}

// dohvaćanje podataka iz baze
function f_get_rows($db, $sql){
    $db->set_charset("utf8");    
    $result = $db->query($sql);
    $rows=[];

    // dok god ima podataka u bazi koji su trazeni oni ce se dodavat u polje rows, dok dode do kraja vraca polje s podacima
    while($row = mysqli_fetch_assoc($result)) {
        $rows[]=$row;
    }    
    return $rows;
}

// instanciranje baze
function f_get_database(){
    $db = new mysqli(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
    if($db->connect_errno){
        throw new Exception("Neuspješna konekcija na bazu.");
    }
    return $db;
}
?>