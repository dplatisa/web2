<?php
$login_err   = '{"h_message":"login redirect","h_errcode":999}';
$request_err = '{"h_message":"request error","h_errcode":998}';
$procedure_err  = '{"h_message":"method error","h_errcode":997}';
$projekt_err  = '{"h_message":"project error","h_errcode":996}';
$logout  = '{"h_message":"Uspješno ste odjavljeni","h_errcode":0}';
$wrong_login = '{"h_message":"Pogrešno korisničko ime ili zaporka. Molimo pokušajte ponovno","h_errcod":111}';
$database_error = '{"h_message":"Neuspješna konekcija na bazu","h_errcod":119}';
$insert_error = '{"h_message":"Neuspješan unos u bazu. Molimo pokušajte ponovno!","h_errcod":113}';
$insert_pass = '{"h_message":"Uspješno ste unijeli u bazu","h_errcod":0}';
$delete_error = '{"h_message":"Neuspješno brisanje korisnika. Molimo pokušajte ponovno!","h_errcod":115}';
$delete_pass = '{"h_message":"Uspješno ste obrisali korisnika!","h_errcod":0}';
$search_error = '{"h_message":"Nema rezultata.","h_errcod":116}'
?>