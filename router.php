<?php 
// baza ne šalje errore i upozorenja
error_reporting(0);

// ruter može zvat samo localhost stranice
header('Access-Control-Allow-Origin:http://localhost');
// dopuštanje cookiea na serveru
header('Access-Control-Allow-Credentials: true');

?>

<?php


require_once('database.php');
session_start();

// spremanje POST ili GET requesta u $injson varijablu

// encode pretvara iz stringa u json
switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $injson = json_encode($_POST);
        break;
    case 'GET':
        $injson = json_encode($_GET);
        break;
    default:
        echo $request_err;
        return;
}
//pretvara json u php varijablu
$in_obj = json_decode($injson);

// logout -> brisanje sessiona
if($in_obj->procedura =="p_logout"){
    session_destroy();
    $_SESSION[]='';
    echo $logout;
    return; 
}

// SESSION ne postoji && PROCEDURA nije 'p_login'
if (!isset($_SESSION["USERNAME"]) && $in_obj->procedura !="p_login") {
       echo $login_err;
       echo $_SESSION["USERNAME"];
       return; 
}

// refresh -> loggiran
if (isset($_SESSION) && $in_obj->procedura == "p_refresh") {
    echo json_encode($_SESSION);
    return; 
}

// spajanje na bazu
//f get database vraca instancu baze
try{
    $db = f_get_database();
 }catch (Exception $e){
    echo $database_error;
    return;
 }

// poziv funkcije
switch ($in_obj->procedura) {
    case 'p_login':
        f_login($db, $in_obj);
        break;
    case 'p_dodaj_event':
        f_save_event($db, $in_obj);
        break;
    case 'p_get_events':
        f_get_events($db, $in_obj);
        break;
    default:
        echo $request_err;
        return;
}
?>